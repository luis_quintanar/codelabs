package com.bbva.qwai;

import com.bbva.qwai.lib.r001.QWAIR001;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Transaction for Codelab
 *
 */
public class QWAIT00101MXTransaction extends AbstractQWAIT00101MXTransaction {

	private static final Logger LOGGER = LoggerFactory.getLogger(QWAIT00101MXTransaction.class);

	@Override
	public void execute() {
		QWAIR001 qwaiR001 = (QWAIR001)getServiceLibrary(QWAIR001.class);
		LOGGER.info("Execution of QWAIT00101ESTransaction");

		List<CustomerDTO> listDTO = null;
		String identityDocumentType = this.getIdentitydocumenttype();
		String identityDocumentNumber = this.getIdentitydocumentnumber();

		if (identityDocumentType == null || identityDocumentNumber == null) {
			listDTO = qwaiR001.execute();
		} else {
			listDTO = qwaiR001.executeDocument(identityDocumentType, identityDocumentNumber);
		}

		if (listDTO == null || listDTO.isEmpty()) {
			setSeverity(Severity.WARN);
		} else {
			this.setEntitylist(listDTO);
		}
	}

	}

}
