package com.bbva.qwai.lib.r001.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QWAIR001Impl extends QWAIR001Abstract {

	private static final Logger LOGGER = LoggerFactory.getLogger(QWAIR001Impl.class);

	@Override
	public List<CustomerDTO> execute() {

		List<CustomerDTO> listDTO = new ArrayList<>();

		CustomerDTO cust1 = new CustomerDTO();
		CustomerDTO cust2 = new CustomerDTO();

		Date date = new Date();

		// First cust1
		cust1.setCustomerId("00001");
		cust1.setFirstName("Jhon");
		cust1.setLastName("Freeman");
		cust1.setNationality("EN");
		cust1.setPersonalTitle("Sr");
		cust1.setGenderId("Male");
		cust1.setIdentityDocumentType("1");
		cust1.setIdentityDocumentNumber("12345678N");
		cust1.setBirthDate(date );
		cust1.setMaritalStatus("Single");

		// Second cust2
		cust2.setCustomerId("00002");
		cust2.setFirstName("Juan");
		cust2.setLastName("Perez");
		cust2.setNationality("ES");
		cust2.setPersonalTitle("Sr");
		cust2.setGenderId("Male");
		cust2.setIdentityDocumentType("1");
		cust2.setIdentityDocumentNumber("98765432N");
		cust2.setBirthDate(date);
		cust2.setMaritalStatus("Single");

		listDTO.add(cust1);
		listDTO.add(cust2);

		return listDTO;
	}

	@Override
	public List<CustomerDTO> executeDocument(String identityDocumentType, String identityDocumentNumber) {
		List<CustomerDTO> listDTO = new ArrayList<>();

		if ("DNI".equals(identityDocumentType) && "12341234N".equals(identityDocumentNumber)) {

			CustomerDTO cust = new CustomerDTO();

			Date date = new Date();
			cust.setCustomerId("00003");
			cust.setFirstName("Carlos");
			cust.setLastName("Martín");
			cust.setNationality("ES");
			cust.setPersonalTitle("Sr");
			cust.setGenderId("Male");
			cust.setIdentityDocumentType("DNI");
			cust.setIdentityDocumentNumber("12341234N");
			cust.setBirthDate(date);
			cust.setMaritalStatus("Single");
			listDTO.add(cust);
		}
		return listDTO;
	}


}
