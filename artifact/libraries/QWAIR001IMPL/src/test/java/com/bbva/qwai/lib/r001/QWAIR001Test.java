package com.bbva.qwai.lib.r001;

import com.bbva.elara.configuration.manager.application.ApplicationConfigurationService;
import com.bbva.elara.domain.transaction.Context;
import com.bbva.elara.domain.transaction.ThreadContext;
import javax.annotation.Resource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.Advised;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:/META-INF/spring/QWAIR001-app.xml",
		"classpath:/META-INF/spring/QWAIR001-app-test.xml",
		"classpath:/META-INF/spring/QWAIR001-arc.xml",
		"classpath:/META-INF/spring/QWAIR001-arc-test.xml" })
public class QWAIR001Test {

	private static final Logger LOGGER = LoggerFactory.getLogger(QWAIR001Test.class);

	@Resource(name = "qwaiR001")
	private QWAIR001 qwaiR001;

	@Resource(name = "applicationConfigurationService")
	private ApplicationConfigurationService applicationConfigurationService;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		ThreadContext.set(new Context());
		getObjectIntrospection();
	}
	
	private Object getObjectIntrospection() throws Exception{
		Object result = this.qwaiR001;
		if(this.qwaiR001 instanceof Advised){
			Advised advised = (Advised) this.qwaiR001;
			result = advised.getTargetSource().getTarget();
		}
		return result;
	}

	@Test
	public void executeTest(){
		LOGGER.info("Executing the test...");
		Assert.assertTrue(qwaiR001.execute().size() > 0);
	}

	@Test
	public void executeDocumentTest(){
		LOGGER.info("Executing the test...");
		Assert.assertEquals(1, qwaiR001.executeDocument("DNI", "12341234N").size());
	}

	@Test
	public void executeDocumentNotFoundTest(){
		LOGGER.info("Executing the test...");
		Assert.assertEquals(0, qwaiR001.executeDocument("DNI", "00000000N").size());
	}
	
}
